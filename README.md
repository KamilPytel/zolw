## Opis

Żółw znany z języka LOGO.

Napisany w Pythonie, wykorzystuje bibliotekę [Matplotlib](https://matplotlib.org/)

Przykłady:

Bransoletka

![](rysunki/bransoletka.png)

Dywan Sierpińskiego

![](rysunki/sierpinski.png)

Krzywa Hilberta

![](rysunki/hilbert.png)

