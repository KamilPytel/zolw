#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''Zadanie 4
https://minilogia.oeiizk.waw.pl/zadania/tresci/05_1.pdf
Wypełnienie pominięte.

Parametry:
    ile: liczba całkowita 1, ..., 10
    żółw: obiekt klasy Żółw
'''

import sys
from zolw import Żółw


SZEROKOŚĆ, WYSOKOŚĆ = 700, 50


def bransoletka(ile, żółw):
    szerokość = SZEROKOŚĆ / ile
    kroczek = WYSOKOŚĆ / 5

    for _ in range(ile):
        figura(szerokość, kroczek, żółw)


def figura(szerokość, kroczek, żółw):
    wysokość = 5 * kroczek
    prostokąt(szerokość - kroczek, wysokość, żółw)

    żółw.podnieś_pisak()
    żółw.naprzód(3 * kroczek)
    żółw.w_lewo(90)
    żółw.naprzód(kroczek)
    żółw.w_prawo(90)
    żółw.opuść_pisak()

    litera_ell(szerokość - 5*kroczek, kroczek, żółw)

    żółw.podnieś_pisak()
    żółw.naprzód(szerokość - 7*kroczek)
    żółw.w_lewo(90)
    żółw.naprzód(3 * kroczek)
    żółw.w_lewo(90)
    żółw.opuść_pisak()

    litera_ell(szerokość - 5*kroczek, kroczek, żółw)

    żółw.podnieś_pisak()
    żółw.wstecz(3 * kroczek)
    żółw.w_lewo(90)
    żółw.naprzód(kroczek)
    żółw.opuść_pisak()

    prostokąt(kroczek, kroczek, żółw)

    żółw.podnieś_pisak()
    żółw.naprzód(3 * kroczek)
    żółw.w_lewo(90)
    żółw.naprzód(kroczek)
    żółw.opuść_pisak()


def litera_ell(szerokość, kroczek, żółw):
    żółw.naprzód(szerokość)
    żółw.w_lewo(90)
    żółw.naprzód(3 * kroczek)
    żółw.w_lewo(90)
    żółw.naprzód(kroczek)
    żółw.w_lewo(90)
    żółw.naprzód(2 * kroczek)
    żółw.w_prawo(90)
    żółw.naprzód(szerokość - kroczek)
    żółw.w_lewo(90)
    żółw.naprzód(kroczek)
    żółw.w_lewo(90)


def prostokąt(szerokość, wysokość, żółw):
    for _ in [1, 2]:
        żółw.naprzód(szerokość)
        żółw.w_lewo(90)
        żółw.naprzód(wysokość)
        żółw.w_lewo(90)


if __name__ == '__main__':
    tolek = Żółw()
    tolek.kolor_pisaka('black')
    bransoletka(8, tolek)
    tolek.rysuj()
