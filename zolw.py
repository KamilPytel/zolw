#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import cos, sin, radians
from matplotlib import pyplot as plt
from segment import Segment


class Żółw:
    '''Żółw z języka LOGO.

    Atrybuty:
        x, y: położenie startowe,
        azymut: kąt do kierunku wschodniego,
        pisak: jeśli True, to opuszczony.
    '''

    def __init__(self, x=0, y=0, azymut=0, pisak=True):
        self._azymut = azymut
        self._pisak = pisak
        self._segment = Segment(x, y, pisak)
        self._segmenty = [self._segment]
        self._styl = {}

    def w_lewo(self, kąt):
        '''Obraca żółwia w lewo o kąt w stopniach.'''
        self._azymut += kąt

    def w_prawo(self, kąt):
        '''Obraca żółwia w prawo o kąt w stopniach.'''
        self._azymut -= kąt

    def naprzód(self, r):
        '''Przesuwa żółwia o r.'''
        x, y = self.pozycja()
        azymut = self._azymut
        x += cos(radians(azymut))*r
        y += sin(radians(azymut))*r
        self._segment.dodaj_pozycję(x, y)

    def wstecz(self, r):
        '''Przesuwa żółwia wstecz o r.'''
        self.w_prawo(180)
        self.naprzód(r)
        self.w_prawo(180)

    def przestaw(self, x, y):
        '''Przestawia żółwia do położenia (x, y).'''
        self._segment.dodaj_pozycję(x, y)

    def pozycja(self):
        '''Zwraca bieżącą pozycję żółwia.'''
        return self._segment.X[-1], self._segment.Y[-1]

    def azymut(self):
        '''Zwraca bieżącą wartość kąta azymut.'''
        return self._azymut

    def _ustaw_stan_pisaka(self, wartość):
        '''Ustawia żądany stan pisaka. Zaczyna nowy segment.'''
        # Nowy segment startuje w bieżącej pozycji żółwia.
        self._pisak = wartość
        x, y = self.pozycja()
        self._segment = Segment(x, y, self._pisak, **self._styl)
        self._segmenty.append(self._segment)

    def _ustaw_styl(self, **styl):
        '''Aktualizuje parametry plt.plot() odpowiadające za styl rysunku.
        Zaczyna nowy segment
        '''
        self._styl.update(**styl)
        x, y = self.pozycja()
        self._segment = Segment(x, y, self._pisak, **self._styl)
        self._segmenty.append(self._segment)

    def podnieś_pisak(self):
        '''Podnosi pisak, żółw nie zostawia śladu na rysunku.'''
        self._ustaw_stan_pisaka(False)

    def opuść_pisak(self):
        '''Opuszcza pisak, żółw zostawia śladu na rysunku.'''
        self._ustaw_stan_pisaka(True)

    def kolor_pisaka(self, kolor):
        '''Ustawia kolor pisaka.

        Lista dostępnych kolorów:
            https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        '''
        self._ustaw_styl(color=kolor)

    def rysuj(self, zachowaj_osie=True, pokaż=True):
        '''Rysuje ścieżkę żółwia.'''

        for segment in self._segmenty:
            if segment.pisak:
                plt.plot(segment.X, segment.Y, **segment.styl)

        if zachowaj_osie:
            ax = plt.gca()
            ax.set_aspect('equal')

        if pokaż:
            plt.show()

